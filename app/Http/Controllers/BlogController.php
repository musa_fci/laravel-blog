<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Lang;
use File;

class BlogController extends Controller
{

    public function blog()
    {
      $id = Session()->get('Admin.id');
      $blogs = DB::table('tbl_blog')->where('user_id', $id)->orderBy('id','desc')->paginate(2);
      $cats = DB::table('tbl_blog')->select('category')->get();

      return view('backend.blog',compact('blogs','cats'));
    }

        /*******************************/
        /*********Blog Store*********/
        /*******************************/

    public function blogStore(Request $request)
    {
      $this->validate($request,[
        'category'      =>    'required',
        'title'         =>    'required',
        'body'          =>    'required',
        'img'           =>    'required',
      ]);

      try{
        $userId = Session()->get('Admin.id');
        $status = ($request->status != '') ? 1 : 0;
        $imageName = time().'.'.$request->img->getClientOriginalExtension();

        DB::table('tbl_blog')->insert([
          'user_id'     =>  $userId,
          'category'    =>  $request->category,
          'title'       =>  $request->title,
          'body'        =>  $request->body,
          'img'         =>  $imageName,
          'status'      =>  $status
        ]);
          $request->img->move(public_path('images'),$imageName);
          return redirect('blog')->with('success','Blog Insert Successfully');

      }catch(\Exception $e){
        return redirect('blog')->with('fail',Lang::get('dbError.'.$e->errorInfo[1]));
      }

    }
              /*******************************/
              /*********Blog Update*********/
              /*******************************/

    public function blogUpdate($id = null,Request $request)
    {
      $status     = ($request->status != '') ? 1: 0;
      try
      {
        if(!empty($request->img))
        {
          $imgs = DB::table('tbl_blog')->select('img')->where('id',$id)->get();
          foreach($imgs as $img){
            $imgPath = public_path('images/').$img->img;
            File::delete($imgPath);
          }

          $imageName  = time().'.'.$request->img->getClientOriginalExtension();

          DB::table('tbl_blog')->where('id',$id)->update([
            'title'   =>  $request->title,
            'body'    =>  $request->body,
            'img'     =>  $imageName,
            'status'  =>  $status
          ]);

          $request->img->move(public_path('images'),$imageName);
          return redirect('blog')->with('success','Blog Data Update Successfully');
        }else{
          DB::table('tbl_blog')->where('id',$id)->update([
            'title'   =>  $request->title,
            'body'    =>  $request->body,
            'status'   =>  $status
          ]);
          return redirect('blog')->with('success','Blog Data Update Successfully');
        }

      }catch(\Exception $e){
        return redirect('blog')->with('fail',Lang::get('dbError.'.$e->errorInfo[1]));
      }
    }


            /*******************************/
            /*********Blog Delete*********/
            /*******************************/

    public function blogDelete($id = null)
    {

      try{

        $imgs = DB::table('tbl_blog')->select('img')->where('id',$id)->get();
        foreach($imgs as $img){
          $imgPath = public_path('images/').$img->img;
          File::delete($imgPath);
        }

        DB::table('tbl_blog')->where('id',$id)->delete();

        return redirect('blog')->with('success','Blog Delete Successfully');

      }catch(\Exception $e){
        return redirect('blog')->with('fail',Lang::get('dbError.'.$e->errorInfo[1]));
      }
    }




}
