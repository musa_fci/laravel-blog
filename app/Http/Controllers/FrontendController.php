<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FrontendController extends Controller
{
    public function index()
    {
      $blogs = DB::table('tbl_blog')->where('status',1)->paginate(3);
      $cats = DB::table('tbl_blog')->select('category')->distinct()->get();
      return view('frontend.index',compact('blogs','cats'));
    }

    public function details($id = null)
    {
      $blog = DB::table('tbl_blog')->where('id', $id)->first();
      return view('frontend.details',compact('blog'));
    }

    public function categoryWaysBlogs($catname = null)
    {
      $blogs = DB::table('tbl_blog')->where('category',$catname)->paginate(3);
      return view('frontend.categoryWaysBlogs',compact('blogs'));
    }



    public function contact()
    {
      return view('frontend.contact');
    }

}
