<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Auth;
use Session;

class UserController extends Controller
{
    public function register()
    {
        return view('backend.register');
    }

    public function userRegister(Request $request)
    {
      $this->validate($request,[
        'name'       => 'required',
        'email'      => 'required|email|unique:tbl_user',
        'password'   => 'required|min:5|max:12|confirmed',
        'address'    => 'required'
      ]);

      DB::table('tbl_user')->insert([
        'name'      => $request->name,
        'email'     => $request->email,
        'password'  => Hash::make($request->password),
        'address'   => $request->address
      ]);
      return redirect('register')->with('success','User Registration Successfully.');
    }


    public function login()
    {
        return view('backend.login');
    }


    public function checkLogin(Request $request)
    {
      $credential = ['email' => $request->email,'password' => $request->password];
      if(Auth::attempt($credential)){
        Session::put('Admin',Auth::user());
        return redirect('admin');
      }else{
        return redirect('login')->with('fail','Login Information Is Wrong...!');
      }
    }


    public function logout()
    {
      Auth::logout();
      Session::forget('Admin');
      return redirect('login')->with('success','Logout Successfull');
    }




}
