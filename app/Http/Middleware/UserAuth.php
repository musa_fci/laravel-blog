<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   if(!Session::has('Admin.id'))
        {
            return redirect('login')->with('fail','You are not Login...!');
        }
        return $next($request);
    }
}
