
; (function () {

	"use strict"; // use strict to start

	/*===================================
	/* Preloader
	/*=================================*/

	$(window).on('load', function () {
		$('body').imagesLoaded(function () {
			$('.loader-inner').fadeOut();
			$('.loader').delay(200).fadeOut('slow').remove();
		});
	});

	$(document).ready(function () {

		/*---------------------------/
		/* Initialize WOW amimation
		/*--------------------------*/

		new WOW({ mobile: false }).init();


		/*-------------------------------
		/* Post Masonry Gallery
		/*-----------------------------*/

		$('.post-gallery').masonry({
			itemSelector: '.gallery-item'
		});

		/*---------------------------------
		/* Navigation Menu 
		/*-------------------------------*/

		$('.nav-bar').on('click', function () {
			$('.main-menu').toggleClass('visible');
			$('.ham').toggleClass('exit');
		})

		$('.parent-menu').on('mouseenter', function () {
			$('.sub-menu', this).fadeIn(700);
		}).on('mouseleave', function () {
			$('.sub-menu', this).hide();
		});

		/*---------------------------------
		/* Mobile Menu 
		/*-------------------------------*/

		$('.mobile-nav .parent-menu > a').on('click', function (event) {
			// Disable Default Event Action
			event.stopPropagation();
			event.preventDefault();

			$('.plus-icon', this).toggleClass('exit');
			$(this).next('.mobile-sub-menu').slideToggle();
		});

		/*---------------------------------
		/* Initializing Tooltips
		/*-------------------------------*/

		jQuery('[data-toggle="tooltip"]').tooltip({ animation: true, delay: { show: 300, hide: 300 } });

		/*---------------------------------
		/* Initializing GMaps (Google Map For Contact Page)
		/*-------------------------------*/

		if ($('#map').length) {
			var map = new GMaps({
				el: '#map',
				lat: -12.043333,	// Define Latitude
				lng: -77.028333,	// Define Longitude
				height: '400px',	// Define Height
				scrollwheel: false 	// Disables Map Zoom on Mouse Scroll
			})
		}
		

		/*----------------------------
		/* Featured Slider
		/*---------------------------*/

		$('.featured-slider').owlCarousel({
			items: 1,
			autoplay: true,
			loop: true,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			nav: true,
			navText: ['<i class="fa fa-angle-double-left"></i>', '<i class="fa fa-angle-double-right"></i>']
		});


		/*-----------------------------
		/*	Related Post Carousel
		/*---------------------------*/

		$('.related-post-carousel').owlCarousel({
			margin: 5,
			responsive: {
				0: {
					items: 1
				},
				640: {
					items: 2
				}
			}
		});

		var relatedPostControl = $('.related-post-carousel');

		$('.nextRelatedPost').on( 'click', function () {
			relatedPostControl.trigger('next.owl.carousel', [1000]);
		});

		$('.prevRelatedPost').on( 'click', function () {
			relatedPostControl.trigger('prev.owl.carousel', [1000]);
		})

		/*-----------------------------
		/*	Blog Post Slider
		/*---------------------------*/
		$('.post-slider').owlCarousel({
			items: 1,
			autoplay: true,
			loop: true,			
			mouseDrag: false,
			touchDrag: false,
			pullDrag: false,
			animateOut: 'fadeOutUp',
			animateIn: 'fadeInUp',
			nav: true,
			navText: ['<i class="fa fa-angle-double-left"></i>', '<i class="fa fa-angle-double-right"></i>']
		});

		/*-----------------------------
		/*	Initialize Facebook 
		/*---------------------------*/

		$(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = '//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6';
			fjs.parentNode.insertBefore(js, fjs);
		} (document, 'script', 'facebook-jssdk'));

		/*-----------------------------
		/*	Initialize Instagram 
		/*---------------------------*/

		$(function () {
			if ($('#instafeed').length > 0) {
				new Instafeed({
					get: 'user',
					userId: '2714397198',												// Add User ID
					clientId: '646a690c4c05430aad736df1d5d4b224',						// Add Client ID
					accessToken: '2714397198.1677ed0.0f507f4477cf460b8a361f226749cd2e',	// Add Access Token
					limit: 9,
					template: '<div class="instagram-img"><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>'
				}).run();
			}
		});

		/*-----------------------------
		/*	Initialize Twitter 
		/*---------------------------*/

		$('.twitter-feed').twittie({
			username: 'shapebootstrap',
			count: 3,
			hideReplies: true,
			dateFormat: '%b %m, %Y',
			template: '<div class="tweet"><p>{{tweet}}</p>{{screen_name}} <span class="tweet-date pull-right"><i class="fa fa-clock-o"></i> {{date}}</span></div>'
		});

		/*--------------------------------------
		/* Initializing Isotope / Home Masonry
		/*------------------------------------*/

		var grid = $('.grid');
		if ($.fn.imagesLoaded && grid.length > 0) {
			imagesLoaded(grid, function () {
				setTimeout(function () {
					grid.isotope({
						itemSelector: '.grid-item',
						filter: '*',
						masonry: {
							// use outer width of grid-sizer for columnWidth
							columnWidth: '.grid-sizer',
						}
					});
					$(window).trigger("resize");
				}, 500);
			});
		}

		/*--------------------------------------
		/* Initialize Disqus Comment Form
		/*------------------------------------*/

		if ($('#disqus_thread').length > 0) {
			/*
			   var disqus_config = function () {
				   this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
				   this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
			   };
			   */
			(function () {  // DON'T EDIT BELOW THIS LINE
				var d = document, s = d.createElement('script');

				s.src = '//drema.disqus.com/embed.js';

				s.setAttribute('data-timestamp', +new Date());
				(d.head || d.body).appendChild(s);
			})();
		}

		/*---------------------------------
		/*	Post Image Gallery 
		/*-------------------------------*/
		$('.post-gallery').magnificPopup({
			delegate: 'a', // child items selector, by clicking on it popup will open
			type: 'image',
			fixedContentPos: false,
			gallery: {
				enabled: true
			},
			mainClass: 'mfp-with-zoom',
			zoom: {
				enabled: true, // By default it's false, so don't forget to enable it

				duration: 300, // duration of the effect, in milliseconds
				easing: 'ease-in-out', // CSS transition easing function
				opener: function (openerElement) {
					return openerElement.is('img') ? openerElement : openerElement.find('img');
				}
			}
		});
	});

	/*---------------------------------
	/*	Send Email Using Ajax
	/*-------------------------------*/

	$('#contactForm').on('submit', function (event) {
		// Disable default event behavior
		event.stopPropagation();
		event.preventDefault();
		var data = $('#contactForm').serialize();

		$.ajax({
			url: '/php/sendemail.php',
			type: 'POST',
			data: data,
			success: function (response) {
				console.log(response);
			}
		})
		.done(function () {
			$.notify("Message Successfully Sent.", { align: 'right', verticalAlign: 'top' });
		})
		.fail(function () {
			$.notify("Message Sending Failed!", { align: 'right', verticalAlign: 'top' });
		});
	});
})(jQuery)