@if(count($errors) > 0)
  @foreach($errors->all() as $error)
  <p class="alert alert-danger" style="text-align: center;">
    {{ $error }}
  </p>
  @endforeach
@endif
