<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{URL::to('/public/backend/assets/img/favicon.ico')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Light Bootstrap Dashboard PRO by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    {!! HTML::style('public/backend/assets/css/bootstrap.min.css') !!}
    {!! HTML::style('public/backend/assets/css/demo.css') !!}

    <!--  Light Bootstrap Dashboard core CSS    -->
    {!! HTML::style('public/backend/assets/css/light-bootstrap-dashboard.css') !!}

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    {!! HTML::style('public/backend/assets/css/pe-icon-7-stroke.css') !!}


    @section('stylesheet')
    @show


</head>
