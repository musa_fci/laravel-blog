<div class="sidebar" data-color="red" data-image="{{URL::to('/public/backend/assets/img/full-screen-image-4.jpg')}}">
    <!--
        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
    -->

    <div class="logo">
        <a href="" class="logo-text">
            Creative Tim
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="" class="logo-text">
            Ct
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{URL::to('/public/backend/assets/img/default-avatar.png')}}"/>
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                  @if(Session()->has('Admin'))
                    {{Session()->get('Admin.name')}}
                  @endif
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li><a href="#">My Profile</a></li>
                        <li><a href="#">Edit Profile</a></li>
                        <li><a href="#">Settings</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav">
            <li class="{{ Request::is('admin') ? 'active' : '' }}">
                <a href="{{URL::to('/admin')}}">
                    <i class="pe-7s-graph"></i>
                    <p>Dashboard</p>
                </a>
            </li>

            <li class="{{ Request::is('blog') ? 'active' : '' }}">
                <a href="{{URL::to('blog')}}">
                    <i class="pe-7s-note2"></i>
                    <p>Blog</p>
                </a>
            </li>

            <!-- <li>
                <a data-toggle="collapse" href="#blog">
                    <i class="pe-7s-note2"></i>
                    <p>Blog
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="blog">
                    <ul class="nav">
                        <li><a href="{{URL::to('blog')}}">Blog Add</a></li>
                    </ul>
                </div>
            </li> -->
            <li>
                <a data-toggle="collapse" href="#componentsExamples">
                    <i class="pe-7s-plugin"></i>
                    <p>Components
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="componentsExamples">
                    <ul class="nav">
                        <li><a href="{{URL::to('/admin/buttons')}}">Buttons</a></li>
                        <li><a href="{{URL::to('/admin/grid')}}">Grid System</a></li>
                        <li><a href="{{URL::to('/admin/icons')}}">Icons</a></li>
                        <li><a href="{{URL::to('/admin/notifications')}}">Notifications</a></li>
                        <li><a href="{{URL::to('/admin/panels')}}">Panels</a></li>
                        <li><a href="{{URL::to('/admin/sweet-alert')}}">Sweet Alert</a></li>
                        <li><a href="{{URL::to('/admin/typography')}}">Typography</a></li>
                    </ul>
                </div>
            </li>

            <li>
                <a data-toggle="collapse" href="#formsExamples">
                    <i class="pe-7s-note2"></i>
                    <p>Forms
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="formsExamples">
                    <ul class="nav">
                        <li><a href="{{URL::to('/admin/form-regular')}}">Regular Forms</a></li>
                        <li><a href="{{URL::to('/admin/form-extended')}}">Extended Forms</a></li>
                        <li><a href="{{URL::to('/admin/form-validation')}}">Validation Forms</a></li>
                        <li><a href="{{URL::to('/admin/form-wizard')}}">Wizard</a></li>
                    </ul>
                </div>
            </li>

            <li>
                <a data-toggle="collapse" href="#tablesExamples">
                    <i class="pe-7s-news-paper"></i>
                    <p>Tables
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="tablesExamples">
                    <ul class="nav">
                        <li><a href="{{URL::to('/admin/table-regular')}}">Regular Tables</a></li>
                        <li><a href="{{URL::to('/admin/table-extended')}}">Extended Tables</a></li>
                        <li><a href="{{URL::to('/admin/table-bootstrap')}}">Bootstrap Table</a></li>
                        <li><a href="{{URL::to('/admin/table-datatables')}}">DataTables.net</a></li>
                    </ul>
                </div>
            </li>

            <li>
                <a data-toggle="collapse" href="#mapsExamples">
                    <i class="pe-7s-map-marker"></i>
                    <p>Maps
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="mapsExamples">
                    <ul class="nav">
                        <li><a href="{{URL::to('/admin/google-map')}}">Google Maps</a></li>
                        <li><a href="{{URL::to('/admin/vector-map')}}">Vector Maps</a></li>
                        <li><a href="{{URL::to('/admin/fullscreen-map')}}">Full Screen Map</a></li>
                    </ul>
                </div>
            </li>

            <li class="{{ Request::is('admin/charts')? 'active' : '' }}">
                <a href="{{URL::to('/admin/charts')}}">
                    <i class="pe-7s-graph1"></i>
                    <p>Charts</p>
                </a>
            </li>

            <li class="{{ Request::is('admin/calendar') ? 'active' : '' }}">
                <a href="{{URL::to('/admin/calendar')}}">
                    <i class="pe-7s-date"></i>
                    <p>Calendar</p>
                </a>
            </li>

            <li>
                <a data-toggle="collapse" href="#pagesExamples">
                    <i class="pe-7s-gift"></i>
                    <p>Pages
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="pagesExamples">
                    <ul class="nav">
                        <li><a href="pages/login.html">Login Page</a></li>
                        <li><a href="pages/register.html">Register Page</a></li>
                        <li><a href="pages/lock.html">Lock Screen Page</a></li>
                        <li><a href="{{URL::to('/admin/user')}}">User Page</a></li
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
