@extends('backend.app')

@section('headerTitle','Fullscreen Maps')

@section('content')
<div id="map" class="full-screen-map"></div>
@endsection


@section('script')
<script>
    $().ready(function(){
        demo.initFullScreenGoogleMap();
    });
</script>
@endsection
