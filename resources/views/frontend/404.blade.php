@extends('frontend.app')

@section('slider')
<!-- BEGIN PAGE BANNER -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-banner">
				<div class="row">
					<div class="col-md-6 col-sm-8">
						<h4 class="text-uppercase banner-title">Page Not Found</h4>
					</div>
					<div class="col-md-6 col-sm-4">
						<div class="text-md-right">
							<ul class="breadcrumb text-uppercase no-padding">
								<li><a href="#">Home</a></li>
								<li class="active">404</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE BANNER -->
@endsection


@section('content')
<!-- BEGIN MAIN CONTENT -->
<div class="col-md-9 col-sm-7 main-content theme-section">
	<div class="post-container inner">
		<div class="content-section post-content-wrap">
			<p class="text-center">
				Error Code: 404
			</p>
			<h1 class="error-title text-center">Oops...</h1>
			<h4 class="error-subtitle text-center">
				Seems like the page is kaput!
			</h4>
			<p class="error-subtitle text-center text-uppercase">
				Try Any Of These Instead
			</p>
			<ul class="possible-links list-inline text-center text-uppercase">
				<li><a href="index.html">Home</a></li>
				<li><a href="about.html">About</a></li>
				<li><a href="blog.html">Blog</a></li>
				<li><a href="contact.html">Contact</a></li>
				<li><a href="archive.html">Archive</a></li>
			</ul>
		</div>
	</div>
	<!-- end post-container -->
</div>
<!-- END MAIN CONTENT -->
@endsection
