<!-- BEGIN FOOTER -->
<footer>
    <div class="container">
        <div class="row">
            <div class="footer-widget footer-logo-wrap col-md-3 col-sm-6">
                <a href="#">
                    <h2 class="footer-logo">Drema</h2>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem quam, quibusdam inventore suscipit omnis.
                </p>
                <img src="{{URL::to('public/frontend/assets/images/about-image.jpg')}}" alt="">
            </div>
            <!-- end footer-logo-wrap -->
            <div class="footer-widget recent-widget col-md-4 col-sm-6">
                <h6 class="widget-title">Recent Posts</h6>
                <ul class="list-undtyled recent-list">
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb1.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">Why It Is Not The Best Time</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> Mar 12, 2016</span> <span class="post-likes"><i class="fa fa-heart-o"></i> 50</span></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb2.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">Five Fantastic Vacation Ideas</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> Feb 02, 2016</span> <span class="post-likes"><i class="fa fa-heart-o"></i> 100</span></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb3.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">7 Exciting Parts Of Carnival</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> Jan 12, 2016</span> <span class="post-likes"><i class="fa fa-heart-o"></i> 20</span></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb4.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">The Reason Why I Love Programs</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> DEC 25, 2015</span> <span class="post-likes"><i class="fa fa-heart-o"></i> 80</span></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- end recent-post-widget -->
            <div class="footer-widget links-widget col-md-2 col-sm-6">
                <h6 class="widget-title">Links</h6>
                <ul class="list-unstyled">
                    <li><a href="about.html">About Drema</a></li>
                    <li><a href="about.html">Archive</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="blog.html">Photo Gallery</a></li>
                    <li><a href="about.html">Terms &amp; Condition</a></li>
                    <li><a href="about.html">Privacy Policy</a></li>
                    <li><a href="blog.html">Contact Us</a></li>
                </ul>
            </div>
            <!-- end twitter-widget -->
            <div class="footer-widget twitter-widget col-md-3 col-sm-6">
                <h6 class="widget-title">Recent Tweets</h6>
                <div class="twitter-feed"></div>
            </div>
            <!-- end twitter-widget -->
        </div>
    </div>
</footer>
<!-- END FOOTER -->

<!-- BEGIN COPYRIGHT -->
<div class="copyright-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <p>Copyright 2016 <a href="#">Themehexa</a>. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</div>
<!-- END COPYRIGHT -->
