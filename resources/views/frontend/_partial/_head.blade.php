<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#000000">
    <meta name="author" content="Themehexa">
    <meta name="description" content="Drema - Personal Blog Template, responsive layout, black and white color">
    <meta name="keywords" content="personal, blog, responsive, black, white, sidebar, masonry, music, video, themehexa, drema">
    <title>Drema - Personal Blog</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::to('public/frontend/assets/images/favicon.png')}}">
    <!-- Material Design Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Font Awesome -->
    {!! HTML::style('public/frontend/assets/css/font-awesome.min.css') !!}
    <!-- Bootstrap core CSS -->
    {!! HTML::style('public/frontend/assets/css/bootstrap.min.css')!!}
    <!-- Material Design Bootstrap -->
    {!! HTML::style('public/frontend/assets/css/mdb.min.css')!!}
    <!-- Magnific Popup -->
    {!! HTML::style('public/frontend/assets/css/magnific-popup.css')!!}
    <!-- Owl Carousel Style -->
    {!! HTML::style('public/frontend/assets/css/owl.carousel.css')!!}
    <!-- Theme Custom Style -->
    {!! HTML::style('public/frontend/assets/css/style.css')!!}

    @section('stylesheet')
    @show

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
