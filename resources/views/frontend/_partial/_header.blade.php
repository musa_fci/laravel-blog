<!-- BEGIN HEADER -->
<header>
    <div class="container">
        <div class="row">
            <!-- begin logo container -->
            <div class="col-md-12 logo-container text-center">
                <a href="index.html">
                    <h1 class="logo text-uppercase">Drema</h1>
                </a>
                <h5 class="subtitle text-uppercase">Your Personal Blog</h5>
            </div>
            <!-- end logo container -->
            <div class="col-md-12">
                <!-- BEGIN HEADER NAVIGATION -->
                <div class="row navigation-container no-margin">
                    <div class="col-sm-6 col-xs-9 no-padding">
                        <ul class="header-social-icons list-inline">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-feed"></i></a></li>
                        </ul>
                    </div>
                    <!-- Begin Mobie Menu Icon -->
                    <div class="col-sm-6 col-xs-3 visible-xs visible-sm header-nav no-padding text-right">
                        <span class="nav-bar" data-toggle="push" data-target="#navbar-mobile">
                                <i class="ham"></i>
                            </span>
                    </div>
                    <!-- End Mobie Menu Icon -->
                    <div class="col-md-6 col-sm-8 hidden-sm hidden-xs header-nav text-right no-padding">
                        <!-- BEGIN HEADER MENU -->
                        <nav>
                            <ul class="list-inline main-menu animated">
                                <li class="parent-menu"><a href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
                                <li class="parent-menu"><a href="{{URL::to('/about')}}">About</a></li>
                                <li class="parent-menu">
                                    <a href="blog.html">Blog</a>
                                    <ul class="sub-menu list-unstyled animated">
                                        <li><a href="blog.html">Blog 2 Grid</a></li>
                                        <li><a href="blog-list.html">Blog List</a></li>
                                        <li><a href="blog-list-sidebar-left.html">Blog Sidebar Left</a></li>
                                        <li><a href="blog-masonry.html">Blog Masonry</a></li>
                                    </ul>
                                </li>
                                <li class="parent-menu">
                                    <a href="#">Pages</a>
                                    <ul class="sub-menu list-unstyled animated">
                                        <li><a href="home-masonry.html">Home Masonry</a></li>
                                        <li><a href="home-fullwidth.html">Home Fullwidth</a></li>
                                        <li><a href="archive.html">Archive</a></li>
                                        <li><a href="details.html">Blog Details</a></li>
                                        <li><a href="404.html">404</a></li>
                                        <li><a href="404-fullwidth.html">404 Fullwidth</a></li>
                                    </ul>
                                </li>
                                <li class="parent-menu"><a href="{{URL::to('/contact')}}">Contact</a>
                                </li>
                            </ul>
                            <span class="nav-bar">
                                    <i class="ham"></i>
                                </span>
                        </nav>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- end header-nav -->
                </div>
                <!-- END HEADER NAVIGATION -->
            </div>
        </div>
    </div>
</header>
<!-- END HEADER -->
