<!-- BEGIN PRELOADER -->
<div class="loader">
    <div class="loader-inner ball-scale">
        <div></div>
    </div>
</div>
<!-- END PRELOADER -->

<!-- BEGIN MOBILE MENU -->
<div id="navbar-mobile" class="mobile-navbar visible-xs visible-sm">
    <ul class="mobile-nav list-unstyled text-uppercase">
        <li class="parent-menu">
            <a href="index.html">Home <span><i class="fa fa-plus plus-icon"></i></span></a>
            <ul class="mobile-sub-menu list-unstyled">
                <li><a href="home-masonry.html">Home Masonry</a></li>
                <li><a href="home-fullwidth.html">Home Fullwidth</a></li>
            </ul>
        </li>
        <li><a href="about.html">About</a></li>
        <li class="parent-menu">
            <a href="blog.html">Blog <span><i class="fa fa-plus plus-icon"></i></span></a>
            <ul class="mobile-sub-menu list-unstyled">
                <li><a href="blog.html">Blog 2 Grid</a></li>
                <li><a href="blog-list.html">Blog List</a></li>
                <li><a href="blog-list-sidebar-left.html">Blog Sidebar Left</a></li>
                <li><a href="blog-masonry.html">Blog Masonry</a></li>
            </ul>
        </li>
        <li class="parent-menu">
            <a href="#">Pages <span><i class="fa fa-plus plus-icon"></i></span></a>
            <ul class="mobile-sub-menu list-unstyled">
                <li><a href="archive.html">Archive</a></li>
                <li><a href="details.html">Blog Details</a></li>
                <li><a href="404.html">404</a></li>
                <li><a href="404-fullwidth.html">404 Fullwidth</a></li>
            </ul>
        </li>
        <li><a href="contact.html">Contact</a></li>
    </ul>
</div>
<!-- END MOBILE MENU -->
