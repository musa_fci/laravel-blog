<!-- SCRIPTS -->
<!-- JQuery -->
{!! HTML::script('public/frontend/assets/js/jquery.min.js') !!}
<!-- Bootstrap core JavaScript -->
{!! HTML::script('public/frontend/assets/js/bootstrap.min.js') !!}
<!-- Material Design Bootstrap -->
{!! HTML::script('public/frontend/assets/js/mdb.js') !!}
<!-- Magnific Popup JavaScript -->
{!! HTML::script('public/frontend/assets/js/jquery.magnific-popup.min.js') !!}
<!-- Owl Carousel JavaScript -->
{!! HTML::script('public/frontend/assets/js/owl.carousel.min.js') !!}
<!-- Masonry JavaScript -->
{!! HTML::script('public/frontend/assets/js/masonry.pkgd.min.js') !!}
<!-- Google Map API -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqSFvtCuol5aO3b7Bwm4309xIO3KZf3FE"></script>
<!-- gmap.js Library -->
{!! HTML::script('public/frontend/assets/js/gmaps.min.js') !!}
<!-- Mobile Push Menu JS -->
{!! HTML::script('public/frontend/assets/js/push-menu.js') !!}
<!-- Instafeed Library For Instagram -->
{!! HTML::script('public/frontend/assets/js/instafeed.min.js') !!}
    <!-- Tweetie JS Library For Twitter feed -->
{!! HTML::script('public/frontend/assets/js/tweetie.min.js') !!}
    <!-- istope JS Library for Masonry -->
{!! HTML::script('public/frontend/assets/js/isotope.pkgd.min.js') !!}
    <!-- Images Loaded JS -->
{!! HTML::script('public/frontend/assets/js/imagesloaded.pkgd.min.js') !!}
    <!-- Theme Custom JavaScript -->
{!! HTML::script('public/frontend/assets/js/functions.js') !!}


@section('script')
@show
