<!-- BEGIN SIDEBAR -->
<div class="col-md-3 col-sm-5 sidebar theme-section">
    <div class="inner">
        <div class="row">
            <div class="search-box col-md-12">
                <input type="text" name="search" id="search" placeholder="SEARCH">
                <i class="search-button fa fa-search"></i>
            </div>
            <!-- end search-box -->
            <!-- SIDEBAR WIDGETS -->
            <div class="widget about-widget col-md-12">
                <h6 class="widget-title">About Me</h6>
                <img src="{{URL::to('public/frontend/assets/images/about-image.jpg')}}" alt="" class="img-about">
                <p class="about-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae sint explicabo velit consequuntur deserunt.
                </p>
                <ul class="social-icons list-inline text-center">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                </ul>
                <div class="text-center">
                    <a href="about.html" class="btn btn-custom">Read More</a>
                </div>
            </div>
            <!-- end about-widget -->
            <div class="widget ad-widget col-md-12">
                <img src="{{URL::to('public/frontend/assets/images/advertisement/boot.jpg')}}" alt="">
            </div>
            <!-- end ad-widget -->
            <div class="widget category-widget col-md-12">
                <h6 class="widget-title">Categories</h6>
                <ul class="list-undtyled category-list">
                  @if(isset($cats))
                    @foreach($cats as $cat)
                      <li>
                        <a href="{{URL::to('/categoryWaysBlogs').'/'.$cat->category}}">
                          <span class="cat-name">{{ $cat->category }}</span>
                        </a>
                        <span class="count">5</span>
                      </li>
                    @endforeach
                  @endif
                </ul>
            </div>
            <!-- end category-widget -->
            <div class="widget ad-widget col-md-12">
                <img src="{{URL::to('public/frontend/assets/images/advertisement/phone.jpg')}}" alt="">
            </div>
            <!-- end ad-widget -->
            <div class="widget popular-widget col-md-12">
                <h6 class="widget-title">Popular</h6>
                <ul class="list-undtyled popular-list">
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb1.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">Why It Is Not The Best Time</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> Mar 12, 2016</span>                                                    <span class="post-likes"><i class="fa fa-heart-o"></i> 50</span></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb2.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">Five Fantastic Vacation Ideas</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> Feb 02, 2016</span>                                                    <span class="post-likes"><i class="fa fa-heart-o"></i> 100</span></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb3.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">7 Exciting Parts Of Carnival</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> Jan 12, 2016</span>                                                    <span class="post-likes"><i class="fa fa-heart-o"></i> 20</span></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <div class="view hm-zoom">
                                        <img class="media-object" src="{{URL::to('public/frontend/assets/images/popular/thumb/thumb4.jpg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="#">
                                    <h6 class="media-heading post-title">The Reason Why I Love Programs</h6>
                                </a>
                                <p><span class="post-date"><i class="fa fa-clock-o"></i> DEC 25, 2015</span>                                                    <span class="post-likes"><i class="fa fa-heart-o"></i> 80</span></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- end popular-widget -->
            <div class="widget featured-widget col-md-12">
                <h6 class="widget-title">Featured</h6>
                <div class="featured-slider">
                    <div class="item">
                        <a href="#"><img src="{{URL::to('public/frontend/assets/images/featured/featured3.jpg')}}" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="{{URL::to('public/frontend/assets/images/featured/featured1.jpg')}}" alt=""></a>
                    </div>
                    <div class="item">
                        <a href="#"><img src="{{URL::to('public/frontend/assets/images/featured/featured2.jpg')}}" alt=""></a>
                    </div>
                </div>
            </div>
            <!-- end featured-widget -->
            <div class="widget instagram-widget col-md-12">
                <h6 class="widget-title">Instagram</h6>
                <div id="instafeed" class="instagram-list clearfix">
                    <!-- This section will be populated by instagram image -->
                </div>
            </div>
            <!-- end instagram-widget -->
            <div class="widget tag-widget col-md-12">
                <h6 class="widget-title">Tags</h6>
                <ul class="tag-list list-inline">
                    <li><a href="#" class="tag-item">Video</a></li>
                    <li><a href="#" class="tag-item">Lifestyle</a></li>
                    <li><a href="#" class="tag-item">Photo</a></li>
                    <li><a href="#" class="tag-item">Travel</a></li>
                    <li><a href="#" class="tag-item">Trip</a></li>
                    <li><a href="#" class="tag-item">Music</a></li>
                    <li><a href="#" class="tag-item">Fizi</a></li>
                    <li><a href="#" class="tag-item">Asia</a></li>
                    <li><a href="#" class="tag-item">Hiking</a></li>
                    <li><a href="#" class="tag-item">Audio</a></li>
                </ul>
            </div>
            <!-- end tag-widget -->
            <div class="widget facebook-widget col-md-12">
                <h6 class="widget-title">Facebook Page</h6>
                <div class="fb-widget fb-page" data-href="https://www.facebook.com/facebook" data-small-header="false" data-adapt-container-width="true"
                    data-hide-cover="false" data-show-facepile="true">
                    <blockquote data-cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
                </div>
            </div>
            <!-- end .facebook-widget -->
        </div>
    </div>
</div>
<!-- END SIDEBAR -->
