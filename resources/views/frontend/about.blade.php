@extends('frontend.app')

@section('slider')
<!-- BEGIN PAGE BANNER -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-banner">
				<div class="row">
					<div class="col-md-6 col-sm-8">
						<h4 class="text-uppercase banner-title">About</h4>
					</div>
					<div class="col-md-6 col-sm-4">
						<div class="text-md-right">
							<ul class="breadcrumb text-uppercase no-padding">
								<li><a href="#">Home</a></li>
								<li class="active">About</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE BANNER -->
@endsection

@section('content')
<!-- BEGIN MAIN CONTENT -->
<div class="col-md-9 col-sm-7 main-content theme-section">
	<div class="post-container inner">
		<img src="{{URL::to('public/frontend/assets/images/about-banner.jpg')}}" alt="" class="post-thumbnail">
		<div class="post-content-wrap">
			<img src="{{URL::to('public/frontend/assets/images/uiface.jpg')}}" alt="" class="uiface center-block img-thumbnail img-circle">

			<h2 class="post-title text-uppercase text-center">I am Simon Gomes</h2>

			<p>
				Cupcake ipsum dolor. Sit amet sugar plum. Cheesecake sesame snaps candy canes lollipop jelly-o donut cotton candy bear claw
				chocolate cake. Jelly beans chocolate bar fruitcake gingerbread jelly-o. Chocolate bonbon liquorice cake liquorice
				topping I love icing cotton candy. Tootsie roll icing marshmallow. Soufflé bear claw toffee I love I love lollipop
				topping gummi bears oat cake. Gingerbread jelly beans sweet chocolate bar topping ice cream candy caramels cupcake.
			</p>
			<p class="quote">
				This is an example of blockquote which is a bit different from normal paragraph text. Lorem ipsum dolor sit amet, consectetur
				adipisicing elit.
			</p>
			<p>
				Bonbon powder danish bear claw powder. Soufflé donut cake gummi bears chocolate cake. Tart tart jelly chupa chups gingerbread.
				Danish jelly beans jujubes tart caramels candy chocolate cake wafer. Cotton candy marzipan donut. Powder pastry halvah
				sweet I love oat cake brownie candy.
			</p>
			<h4>Animi, id est laborum et dolorum fuga.</h4>
			<p>At vero eos et accusamus. Quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Do eiusmod tempor
				incididunt ut labore et dolore magna aliqua. Nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit
				qui in ea voluptate velit esse quam.</p>
			<ol>
				<li>
					Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.
				</li>
				<li>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit.
				</li>
				<li>
					Et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.
				</li>
			</ol>

			<div class="row about-section">
				<div class="col-md-7">

					<div class="content-section">
						<h5 class="section-title text-uppercase">Hobbies</h5>
						<ul class="list-inline">
							<li>Photography,</li>
							<li>Hiking,</li>
							<li>Traveling,</li>
							<li>Bike Riding,</li>
							<li>Sky Diving</li>
						</ul>
					</div>

					<div class="content-section">
						<h5 class="section-title text-uppercase">Skills</h5>
						<ul class="">
							<li>
								<p class="text-uppercase"><b>Designing</b></p>
								<div class="progress dr-custom-progress" data-toggle="tooltip" data-placement="top" data-original-title="70%">
									<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
									</div>
								</div>
							</li>
							<li>
								<p class="text-uppercase"><b>Photography</b></p>
								<div class="progress dr-custom-progress" data-toggle="tooltip" data-placement="top" data-original-title="80%">
									<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
									</div>
								</div>
							</li>
							<li>
								<p class="text-uppercase"><b>Guitar</b></p>
								<div class="progress dr-custom-progress" data-toggle="tooltip" data-placement="top" data-original-title="50%">
									<div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
									</div>
								</div>
							</li>
							<li>
								<p class="text-uppercase"><b>Drifting</b></p>
								<div class="progress dr-custom-progress" data-toggle="tooltip" data-placement="top" data-original-title="60%">
									<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%">
									</div>
								</div>
							</li>
							<li>
								<p class="text-uppercase"><b>Sky Diving</b></p>
								<div class="progress dr-custom-progress" data-toggle="tooltip" data-placement="top" data-original-title="80%">
									<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
									</div>
								</div>
							</li>
						</ul>
					</div>

				</div>
				<div class="col-md-5">
					<img src="{{URL::to('public/frontend/assets/images/about-thumb.jpg')}}" class="img-fluid img-thumbnail hoverable img-no-radius" alt="">
				</div>
			</div>
			<!-- end about-section -->
		</div>
		<!-- end post-content-wrap -->
	</div>
	<!-- end post-container -->
</div>
<!-- END MAIN CONTENT -->
@endsection
