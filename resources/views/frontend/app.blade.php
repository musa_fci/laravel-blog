<!DOCTYPE html>
<html lang="en">

  @include('frontend._partial._head')

  <body>

      @include('frontend._partial._mobileNav')

      <!-- BEGIN WRAP -->
      <div id="wrap" class="home">
          @include('frontend._partial._header')

          @section('slider')
          @show


          <!-- BEGIN MEIN CONTAINER -->
          <div id="main-container" class="body-container container">
              <div class="row">

                @section('content')
                @show

                @include('frontend._partial._sidebar')

              </div>
          </div>
          <!-- END MAIN CONTAINER -->

          @include('frontend._partial._footer')

      </div>
      <!-- END WRAP -->

  @include('frontend._partial._script')

  </body>

</html>
