@extends('frontend.app')

@section('slider')
<!-- BEGIN PAGE BANNER -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-banner">
				<div class="row">
					<div class="col-md-6 col-sm-8">
						<h4 class="text-uppercase banner-title">Blog Listing</h4>
					</div>
					<div class="col-md-6 col-sm-4">
						<div class="text-md-right">
							<ul class="breadcrumb text-uppercase no-padding">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li class="active">Blog List</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE BANNER -->
@endsection



@section('content')
<!-- BEGIN MAIN CONTENT / POST LIST -->
<div class="col-md-9 col-sm-7 main-content theme-section">

	<article class="post-container default-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail view overlay">
					<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-1.jpg')}}" alt="">
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#Lifestyle</a></li>
					</ul>
					<div class="mask waves-effect waves-light"></div>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">History of nomad</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
	<!-- end post-container default-post -->

	<article class="post-container slider-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail">
					<div class="post-slider">
						<div class="item" data-thumb='Test Thumbnail'>
							<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-4.jpg')}}" alt="">
						</div>
						<div class="item">
							<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-5.jpg')}}" alt="">
						</div>
						<div class="item">
							<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-6.jpg')}}" alt="">
						</div>
					</div>
					<!-- end post-slider -->
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#photo</a></li>
						<li><a href="#">#gallery</a></li>
					</ul>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">Photo Gallery Post</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
	<!-- end post-container slider-post -->

	<article class="post-container default-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail view overlay">
					<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-3.jpg')}}" alt="">
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#travel</a></li>
					</ul>
					<div class="mask waves-effect waves-light"></div>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">Travel Be Like in 100 Years?</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
	<!-- end post-container default-post -->

	<article class="post-container video-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail">
					<iframe src="https://www.youtube.com/embed/dtiyZBY8W_E?showinfo=0&amp;iv_load_policy=3" allowfullscreen></iframe>
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#video</a></li>
						<li><a href="#">#youtube</a></li>
					</ul>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">Youtube Video Post</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
	<!-- end post-container video-post -->

	<article class="post-container default-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail view overlay">
					<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-2.jpg')}}" alt="">
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#fun</a></li>
						<li><a href="#">#asia</a></li>
					</ul>
					<div class="mask waves-effect waves-light"></div>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">Lost in Nowhere</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
	<!-- end post-container default-post -->

	<article class="post-container music-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail">
					<iframe src="https://w.soundcloud.com/player/?visual=true&amp;url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F227444763&amp;show_artwork=true"></iframe>
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#Music</a></li>
					</ul>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">Sample Music Post</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
	<!-- end post-container music-post -->

	<article class="post-container default-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail view overlay">
					<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-1.jpg')}}" alt="">
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#Lifestyle</a></li>
					</ul>
					<div class="mask waves-effect waves-light"></div>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">History of nomad</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
	<!-- end post-container default-post -->

	<article class="post-container default-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail view overlay">
					<img src="{{URL::to('public/frontend/assets/images/blog/blog-img-3.jpg')}}" alt="">
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#travel</a></li>
					</ul>
					<div class="mask waves-effect waves-light"></div>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="details.html">Travel Be Like in 100 Years?</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est impedit illum incidunt alias voluptas animi.
					</p>
					<a href="details-gallery.html" class="btn btn-custom">Read More</a>
				</div>
			</div>
			<!-- end post-content-wrap -->
		</div>
	</article>
	<!-- end post-container default-post -->


	<!-- BEGIN PAGINATION -->
	<div class="row pagination-container">
		<div class="col-md-3 col-xs-6 text-left">
			<a href="#" class="btn btn-custom btn-custom-round"><span aria-hidden="true">&larr;</span> New Posts</a>
		</div>
		<div class="col-md-6 hidden-sm hidden-xs text-center">
			<ul class="pagination">
				<li><a href="#">1</a></li>
				<li class="active"><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
			</ul>
		</div>
		<div class="col-md-3 col-xs-6 text-right">
			<a href="#" class="btn btn-custom btn-custom-round">Old Posts <span aria-hidden="true">&rarr;</span></a>
		</div>
	</div>
	<!-- END PAGINATION -->

</div>
<!-- END MAIN CONTENT / POST LIST -->
@endsection
