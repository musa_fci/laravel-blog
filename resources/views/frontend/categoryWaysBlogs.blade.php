@extends('frontend.app')

@section('slider')
<!-- BEGIN PAGE BANNER -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-banner">
				<div class="row">
					<div class="col-md-6 col-sm-8">
						<h4 class="text-uppercase banner-title">Blog Listing</h4>
					</div>
					<div class="col-md-6 col-sm-4">
						<div class="text-md-right">
							<ul class="breadcrumb text-uppercase no-padding">
								<li><a href="#">Home</a></li>
								<li><a href="#">Blog</a></li>
								<li class="active">Blog List</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE BANNER -->
@endsection



@section('content')
<!-- BEGIN MAIN CONTENT / POST LIST -->
<div class="col-md-9 col-sm-7 main-content theme-section">

@if(isset($blogs))
@foreach($blogs as $blog)
	<article class="post-container default-post inner">
		<div class="row">
			<div class="col-md-6">
				<div class="post-thumbnail view overlay">
					<img src="{{URL::to('public/images').'/'.$blog->img}}" alt="">
					<ul class="post-categories list-inline text-uppercase">
						<li><a href="#">#{{ $blog->category }}</a></li>
					</ul>
					<div class="mask waves-effect waves-light"></div>
				</div>
				<!-- end post-thumbnail -->
			</div>
			<div class="col-md-6">
				<div class="post-content-wrap">
					<h2 class="post-title text-uppercase"><a href="{{URL::to('details').'/'.$blog->id}}">{{ $blog->title }}</a></h2>
					<ul class="post-meta list-inline">
						<li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
						<li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
					</ul>
					<p>
						{{ substr($blog->body,0,100) }}
					</p>
					<a href="{{URL::to('details').'/'.$blog->id}}" class="btn btn-custom">Read More</a>
				</div>
				<!-- end post-content-wrap -->
			</div>
		</div>
	</article>
@endforeach
@endif
	<!-- end post-container default-post -->

	<!-- BEGIN PAGINATION -->
	<div class="row pagination-container">
		<div class="col-md-3 col-xs-6 text-left">
			<!-- <a href="#" class="btn btn-custom btn-custom-round"><span aria-hidden="true">&larr;</span> New Posts</a> -->
		</div>
		<div class="col-md-6 hidden-sm hidden-xs text-center">
			{{$blogs->links()}}
		</div>
		<div class="col-md-3 col-xs-6 text-right">
			<!-- <a href="#" class="btn btn-custom btn-custom-round">Old Posts <span aria-hidden="true">&rarr;</span></a> -->
		</div>
	</div>
	<!-- END PAGINATION -->

</div>
<!-- END MAIN CONTENT / POST LIST -->
@endsection
