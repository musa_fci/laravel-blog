@extends('frontend.app')

@section('slider')
<!-- BEGIN PAGE BANNER -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-banner">
                <div class="row">
                    <div class="col-md-6 col-sm-8">
                        <h4 class="text-uppercase banner-title">Contact</h4>
                    </div>
                    <div class="col-md-6 col-sm-4">
                        <div class="text-md-right">
                            <ul class="breadcrumb text-uppercase no-padding">
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li class="active">Contact</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BANNER -->
@endsection


@section('content')
<!-- BEGIN MAIN CONTENT -->
<div class="col-md-9 col-sm-7 main-content theme-section">
    <div class="post-container inner wow fadeIn">
        <div class="row">
            <div class="col-md-6 contact-details">
                <div class="content-section post-content-wrap no-xs-padding-bottom">
                    <h5 class="section-title text-uppercase">Contact Info</h5>
                    <p>At vero eos et accusamus. Quia consequuntur magni dolores eos qui ratione voluptatem
                        sequi nesciunt.</p>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-home"></i> 42 Dimond Street, Nomans Land, United States
                        </li>
                        <li><i class="fa fa-phone"></i> +880 1247 012523</li>
                        <li><i class="fa fa-envelope"></i> info@drema.com</li>
                        <li>
                            <i class="fa fa-link"></i> <a href="http://www.drema.com/">www.drema.com</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end contact-details -->
            <div class="col-md-6 contact-form"><br>
              @include('_partial._success')
              @include('_partial._fail')
              @include('_partial._error')
                <div class="content-section post-content-wrap">
                    <h5 class="section-title text-uppercase">Leave A Message</h5>
                    <form action="{{URL::to('/contact')}}" method="post">
                        {{ csrf_field() }}
                        <input class="form-control" name="name" placeholder="Name" required="" type="text">
                        <input class="form-control"  name="email" placeholder="Email" required="" type="email">
                        <textarea class="materialize-textarea form-control"  name="message" placeholder="Message" required=""></textarea>
                        <button class="btn btn-custom btn-custom-big"  type="submit">Send</button>
                    </form>
                </div>
            </div>
            <!-- end contact-form -->
            <div class="col-md-12">
                <div class="dr-map" id="map"></div>
            </div>
            <!-- end dr-map -->
        </div>
    </div>
    <!-- end post-container -->
</div>
<!-- END MAIN CONTENT -->
@endsection
