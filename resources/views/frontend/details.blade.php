@extends('frontend.app')

@section('slider')
<!-- BEGIN PAGE BANNER -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-banner">
                <div class="row">
                    <div class="col-md-6 col-sm-8">
                        <h4 class="text-uppercase banner-title">Blog Details</h4>
                    </div>
                    <div class="col-md-6 col-sm-4">
                        <div class="text-md-right">
                            <ul class="breadcrumb text-uppercase no-padding">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Blog</a></li>
                                <li class="active">Blog Details</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BANNER -->
@endsection

@section('content')
<!-- BEGIN MAIN CONTENT / POST LIST -->
<div class="col-md-9 col-sm-7 main-content theme-section">
    <div class="row">
        <div class="col-md-12">

            @if(isset($blog))
                <div class="post-container default-post inner wow fadeIn">
                  <img src="{{URL::to('public/images').'/'.$blog->img}}" alt="" class="post-thumbnail">
                  <div class="post-content-wrap text-center">
                      <span class="post-badge"><i class="fa fa-image"></i></span>
                      <ul class="post-categories list-inline text-uppercase">
                          <li><a href="#">#Travel</a></li>
                      </ul>
                      <h2 class="post-title text-uppercase">{{ $blog->title }}</h2>
                      <ul class="post-meta list-inline">
                          <li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
                          <li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i> Jan 12, 2016</a></li>
                      </ul>
                      <div class="post-content text-left">
                          <p>
                              {{ $blog->body }}
                          </p>

                      </div>
                      <!-- end post-content -->
                      <div class="post-tags">
                          <ul class="list-inline post-tag-list">
                              <li><a href="#" class="tag">travel</a></li>
                              <li><a href="#" class="tag">lifestyle</a></li>
                              <li><a href="#" class="tag">europe</a></li>
                              <li><a href="#" class="tag">trip</a></li>
                          </ul>
                      </div>
                      <!-- end post-tags -->
                      <div class="post-bottom-content row">
                          <div class="col-md-6 text-md-left">
                              <ul class="social-icons list-inline">
                                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                  <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                              </ul>
                          </div>
                          <div class="col-md-6 text-md-right">
                              <ul class="like-comment list-inline text-uppercase">
                                  <li><a href="#disqus_thread" class="post-comments"><i class="fa fa-comment-o"></i> Comments</a></li>
                                  <li><a href="#" class="post-likes"><i class="fa fa-heart-o"></i> Like</a></li>
                              </ul>
                          </div>
                      </div>
                      <!-- end post-bottom-content -->
                  </div>
                  <!-- end post-content-wrap -->
                </div>
            @endif


            <!-- end post-container default-post -->
            <div class="next-prev-button row">
                <div class="col-md-4 col-xs-6 text-md-left">
                    <a href="#" class="btn btn-custom btn-custom-round"><span aria-hidden="true">&larr;</span> Previous</a>
                </div>
                <div class="col-md-4 col-xs-6 col-md-push-4 text-md-right">
                    <a href="#" class="btn btn-custom btn-custom-round">Next <span aria-hidden="true">&rarr;</span></a>
                </div>
            </div>
            <!-- end next-prev-button -->
        </div>
        <!-- BEGIN POST AUTHOR -->
        <div class="col-md-12">
            <div class="post-author row wow fadeIn">
                <div class="col-md-2 text-md-left text-center">
                    <img src="{{URL::to('public/frontend/assets/images/author-face.jpg')}}" alt="" class="post-author-img img-responsive img-thumbnail img-circle">
                </div>
                <div class="col-md-10">
                    <h6 class="author-name text-uppercase text-md-left text-center">Bryan Jackson</h6>
                    <p class="text-md-left text-center">
                        Facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Ut enim ad minima veniam, quis nostrum exercitationem
                        ullam corporis suscipit laboriosam. Architecto beatae vitae dicta sunt explicabo.
                    </p>
                    <div class="autor-social-links text-md-left text-center">
                        <ul class="list-inline">
                            <li>
                                <a href="#">
                                    <span class="social-icon">
                                                <i class="fa fa-facebook"></i>
                                            </span>
                                    <span class="social-text">
                                                facebook
                                            </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="social-icon">
                                                <i class="fa fa-twitter"></i>
                                            </span>
                                    <span class="social-text">
                                                twitter
                                            </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="social-icon">
                                                <i class="fa fa-pinterest-p"></i>
                                            </span>
                                    <span class="social-text">
                                                pinterest
                                            </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="social-icon">
                                                <i class="fa fa-instagram"></i>
                                            </span>
                                    <span class="social-text">
                                                instagram
                                            </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END POST AUTHOR -->
        <!-- BEGIN RELATED POST -->
        <div class="col-md-12">
            <div class="related-post wow fadeIn">
                <h5 class="section-title text-uppercase">You may also like</h5>
                <div class="carousel-controller">
                    <span class="prevRelatedPost"><i class="fa fa-long-arrow-left"></i></span>
                    <span class="nextRelatedPost"><i class="fa fa-long-arrow-right"></i></span>
                </div>
                <div class="related-post-list related-post-carousel">
                    <div class="item">
                        <a href="#">
                            <div class="view overlay hm-black-light">
                                <img src="{{URL::to('public/frontend/assets/images/related/related1.jpg')}}" alt="" class="related-post-img">
                                <div class="mask flex-center"></div>
                            </div>
                            <h4 class="related-post-title text-uppercase text-center">Shopping Tips</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <div class="view overlay hm-black-light">
                                <img src="{{URL::to('public/frontend/assets/images/related/related2.jpg')}}" alt="" class="related-post-img">
                                <div class="mask flex-center"></div>
                            </div>
                            <h4 class="related-post-title text-uppercase text-center">Quality Times</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <div class="view overlay hm-black-light">
                                <img src="{{URL::to('public/frontend/assets/images/related/related3.jpg')}}" alt="" class="related-post-img">
                                <div class="mask flex-center"></div>
                            </div>
                            <h4 class="related-post-title text-uppercase text-center">Night at Jungle</h4>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <div class="view overlay hm-black-light">
                                <img src="{{URL::to('public/frontend/assets/images/related/related4.jpg')}}" alt="" class="related-post-img">
                                <div class="mask flex-center"></div>
                            </div>
                            <h4 class="related-post-title text-uppercase text-center">Happiest Memories</h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END RELATED POST -->
        <div id="disqus_thread" class="col-md-12">Disqus Thread</div>
        <!-- <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript> -->
    </div>
</div>
<!-- END MAIN CONTENT / POST LIST -->
@endsection
