@extends('frontend.app')


@section('slider')
<!-- BEGIN HERO SLIDER -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!-- Carousel -->
            <div id="banner" class="carousel banner-slider slide carousel-fade" data-ride="carousel" data-interval="8000">
                <!-- slide wrapper -->
                <div class="carousel-inner" role="listbox">
                    <!-- First slide -->
                    <div class="item active">
                        <div class="view overlay">
                            <a><img src="{{URL::to('public/frontend/assets/images/banner/banner1.jpg')}}" class="img-responsive" alt="slide1">
                                <div class="mask pattern-5 waves-effect waves-light"></div>
                            </a>
                            <div class="carousel-caption hidden-xs">
                                <div class="caption-inner animated flipInX">
                                    <ul class="slider-post-categories list-inline text-uppercase">
                                        <li><a href="#">#Travel</a></li>
                                    </ul>
                                    <h4 class="slider-post-title text-uppercase">History Of Nomad</h4>
                                    <p class="slider-post-date text-uppercase">
                                        <small>Jan 23, 2016</small>
                                    </p>
                                    <a href="#" class="btn btn-custom">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.item -->
                    <!-- Second slide -->
                    <div class="item">
                        <div class="view overlay">
                            <a><img src="{{URL::to('public/frontend/assets/images/banner/banner2.jpg')}}" class="img-responsive" alt="">
                                <div class="mask pattern-5 waves-effect waves-light"></div>
                            </a>
                            <div class="carousel-caption hidden-xs">
                                <div class="caption-inner animated flipInX">
                                    <ul class="slider-post-categories list-inline text-uppercase">
                                        <li><a href="#">#Travel</a></li>
                                    </ul>
                                    <h4 class="slider-post-title text-uppercase">Once Upon A Time</h4>
                                    <p class="slider-post-date text-uppercase">
                                        <small>Jan 23, 2016</small>
                                    </p>
                                    <a href="#" class="btn btn-custom">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.item -->
                </div>
                <!-- /.carousel-inner -->
                <!-- Controls -->
                <a class="left carousel-control new-control" href="#banner" role="button" data-slide="prev">
                    <span class="fa fa-angle-double-left waves-effect waves-light animated"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control new-control" href="#banner" role="button" data-slide="next">
                    <span class="fa fa-angle-double-right waves-effect waves-light animated"></span>
                    <span class="sr-only">Previous</span>
                </a>
            </div>
            <!-- /.carousel -->
        </div>
    </div>
</div>
<!-- END HERO SLIDER -->
@endsection


@section('content')
<!-- BEGIN MAIN CONTENT / POST LIST -->
<div class="col-md-9 col-sm-7 main-content theme-section">
    <div class="row">
        <div class="col-md-12">

          @if(isset($blogs))
            @foreach($blogs as $blog)
            <article class="post-container default-post inner wow fadeIn">
                <a href="{{URL::to('details').'/'.$blog->id}}">
                  <img src="{{URL::to('public/images').'/'.$blog->img}}" alt="" class="post-thumbnail">
                </a>
                <div class="post-content-wrap text-center">
                    <span class="post-badge"><i class="fa fa-image"></i></span>
                    <ul class="post-categories list-inline text-uppercase">
                        <li><a href="{{URL::to('/categoryWaysBlogs').'/'.$blog->category}}">#{{$blog->category}}</a></li>
                    </ul>
                    <!-- end post-categories -->
                    <h2 class="post-title text-uppercase"><a href="{{URL::to('details').'/'.$blog->id}}">{{$blog->title}}</a></h2>
                    <ul class="post-meta list-inline">
                        <li><a href="#" class="post-author text-uppercase"><i class="fa fa-user"></i> Simon Gomes</a></li>
                        <li><a href="#" class="post-date text-uppercase"><i class="fa fa-calendar-o"></i>{{$blog->created_at}}</a></li>
                    </ul>
                    <!-- end post-meta -->
                    <p class="post-content">
                        {{ substr($blog->body, 0,200) }}
                    </p>
                    <!-- end post-content -->
                    <a href="{{URL::to('details').'/'.$blog->id}}" class="btn btn-custom">Read More</a>
                    <div class="post-bottom-content row">
                        <div class="col-md-6 text-md-left">
                            <ul class="social-icons list-inline">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <ul class="like-comment list-inline text-uppercase">
                                <li><a href="#" class="post-comments"><i class="fa fa-comment-o"></i> Comments</a></li>
                                <li><a href="#" class="post-likes"><i class="fa fa-heart-o"></i> Like</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end post-bottom-content -->
                </div>
                <!-- end post-content-wrap -->
            </article>
            @endforeach
          @endif

          {{$blogs->links()}}

        </div>
    </div>
</div>
<!-- END MAIN CONTENT / POST LIST -->
@endsection
